// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
    macros_path = get_absolute_file_path("buildmacros.sce");

    blocks = ["wgserialxcosio"];

    tbx_build_macros(TOOLBOX_NAME, macros_path);
    tbx_build_blocks(toolbox_dir, blocks);
    tbx_build_pal_loader(TOOLBOX_TITLE, blocks, toolbox_dir, macros_path)
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

