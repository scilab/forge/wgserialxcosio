// *********************************************************************
// Base64 and CRC library for SCICOS block, WIN32/UNIX
// Copyright (C) 2016  Weichinger Klaus, snaky.1@gmx.at
// Project web-page: http://bioe.sourceforge.net
//
// License:  GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
// http://www.gnu.org/licenses/
// *********************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *********************************************************************
// Dedication
// **********
//
//  This small piece of code was developed before year
//  2012 and now i publish and dedicate it our beloved
//  son Gregor, who died suddenly 2015.
//
//     +-----+
//     |      \    #
//     |Gregor +---#--+
//    _|___ Weichinger \
//   /     \  2012-2015 |
//  |   O   |----/   \--+
//   \     /     \   /
//  ###############################

#ifndef _binarysource_h_
#define _binarysource_h_

#define BINARYSOURCE_EOF      1
#define BINARYSOURCE_OK       0
#define BINARYSOURCE_ERROR    255

struct binarysource {
  unsigned char *buffer;
  unsigned short length;
  unsigned short index;
  unsigned char finished;
  unsigned char base64index;
};

void binarysource_init(struct binarysource *source, unsigned char *buffer, unsigned short length)
{
  source->buffer=buffer;
  source->length=length;
  source->index=0;
  source->base64index=0;
  source->finished=0;
}

unsigned char binarysource_putnextbase64(struct binarysource *source, unsigned char c)
{
  unsigned char inputvalue=255;
  unsigned char res=BINARYSOURCE_OK;
  if (c=='=' || source->finished != 0)
  {
    res=BINARYSOURCE_EOF;
    source->finished=1;
  }
  else if (c>='a') {
    inputvalue=c-'a'+26;
  } else if (c>='A')
  {
    inputvalue=c-'A';
  }
  else if (c>='0')
  {
    inputvalue=c-'0'+52;
  }
  else if (c=='+')
  {
    inputvalue=62;
  }
  else if (c=='/')
  {
    inputvalue=63;
  }
  else
  {
    res=BINARYSOURCE_ERROR;
  }
  if (res==BINARYSOURCE_OK)
  {
    if (source->index<source->length)
    {
      switch(source->base64index)
      {
        case 0:
          source->buffer[source->index]=(inputvalue<<2)&0xFC;
          source->base64index=1;
          break;
        case 1:
          source->buffer[source->index]|=(inputvalue>>4)&0x03;
          source->index++;
          source->buffer[source->index]=(inputvalue<<4)&0xF0;
          res=BINARYSOURCE_OK;
          source->base64index=2;
          break;
        case 2:
          source->buffer[source->index]|=(inputvalue>>2)&0x0F;
          source->index++;
          source->buffer[source->index]=(inputvalue<<6)&0xC0;
          res=BINARYSOURCE_OK;
          source->base64index=3;
          break;
        case 3:
          source->buffer[source->index]|=(inputvalue)&0x3F;
          source->index++;
          res=BINARYSOURCE_OK;
          source->base64index=0;
          break;
        default:
          break;
      }
    }
    else
    {
      res=BINARYSOURCE_ERROR;
    }
  }
  return res;
}

unsigned char binarysource_getnextbase64(struct binarysource *source, unsigned char *c)
{
  unsigned char res=BINARYSOURCE_ERROR;
  unsigned char x;
  static const char base64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  if (source->index<source->length && source->finished == 0)
  {
    switch(source->base64index)
    {
      case 0:
        x=source->buffer[source->index]>>2;
        *c=base64[x];
        res=BINARYSOURCE_OK;
        source->base64index=1;
        break;
      case 1:
        x=(source->buffer[source->index]<<4)&0x30;
        source->index++;
        x|=(source->buffer[source->index]>>4)&0x0F;
        *c=base64[x];
        res=BINARYSOURCE_OK;
        source->base64index=2;
        break;
      case 2:
        x=(source->buffer[source->index]<<2)&0x3C;
        source->index++;
        x|=(source->buffer[source->index]>>6)&0x03;
        *c=base64[x];
        res=BINARYSOURCE_OK;
        source->base64index=3;
        break;
      case 3:
        x=source->buffer[source->index]&0x3F;
        source->index++;
        *c=base64[x];
        res=BINARYSOURCE_OK;
        source->base64index=0;
        break;
      default:
        break;
    }
  }
  else
  {
    switch(source->base64index)
    {
      case 0:
        source->finished=1;
        res=BINARYSOURCE_EOF;
        break;
      case 1:
        res=BINARYSOURCE_ERROR; // this should never happen here
        break;
      case 2:
        *c='=';
        res=BINARYSOURCE_OK;
        source->base64index=3;
        break;
      case 3:
        *c='=';
        res=BINARYSOURCE_OK;
        source->base64index=0;
        break;
    }
  }
  return res;
}

unsigned short binarysource_getlength(struct binarysource *source)
{
  return source->index;
}

#endif
