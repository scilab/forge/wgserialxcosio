wgserialxcosio 16.04
========================================
author: Dipl.-Ing. Klaus Weichinger
email:  snaky.1@gmx.at
web:    bioe.sourceforge.net

Function
------------------
  This toolbox provides a XCos block to interface real
  hardware via the serial port to an XCos simulation.
  The block uses a generic approach so that each kind
  and amount of inputs and outputs can be exchanged with
  each embedded device that provides a serial interface.

  Further details can be found within the help system entry.

Dedication
------------------
  This small piece of code was developed before year
  2012 and now i publish and dedicate it our beloved
  son Gregor, who died suddenly 2015.

```
     +-----+
     |      \    #
     |Gregor +---#--+
    _|___ Weichinger \
   /     \  2012-2015 |
  |   O   |----/   \--+
   \     /     \   /
  ###############################
```

Licensing
------------------
  The software of this project is distributed under
  the terms and conditions of the
  GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007.

  The full text version of the license is attached
  within the license.txt file.
