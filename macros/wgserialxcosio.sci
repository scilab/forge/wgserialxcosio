// *********************************************************************
// Serial Communication Interface Scicos-Block
// Copyright (C) 2016  Weichinger Klaus, snaky.1@gmx.at
// Project web-page: http://bioe.sourceforge.net
//
// License:  GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
// http://www.gnu.org/licenses/
// *********************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *********************************************************************
// Dedication
// **********
//
//  This small piece of code was developed before year
//  2012 and now i publish and dedicate it our beloved
//  son Gregor, who died suddenly 2015.
//
//     +-----+
//     |      \    #
//     |Gregor +---#--+
//    _|___ Weichinger \
//   /     \  2012-2015 |
//  |   O   |----/   \--+
//   \     /     \   /
//  ###############################

function [x, y, typ] = wgserialxcosio(job, arg1, arg2)
x = []; y = []; typ = [];
select job
       case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
case 'set' then
  x = arg1;
  model = arg1.model; graphics = arg1.graphics;
  exprs = graphics.exprs;
  parametercaptionsorg=..
    ['Connection (COM-Port, IP)';
     'Parameter (Baudrate, TCP-Port)';
     'Output signals frame format';
     'Input signals frame format';
     'Using threading (1...yes, 0...no)';
     'Threading priority'];
  parametercaptions=parametercaptionsorg
  while %t do
	  // acquire parameters in dialog box

    [ok,comport,baudrate,outputformat,inputformat,usethreading,threadingpriority,exprs] =..
        getvalue(['WG Serial XCOS IO (by Weichinger Klaus, snaky.1@gmx.at)';..
                  'http://bioe.sourceforge.net';..
                  '';..
                  'The following characters are used to define';..
                  'the format of outgoing and incoming frames:';..
                  '  b ... unsigned 8bit value (0...255)';..
                  '  w ... unsigned 16bit value (0...655535)';..
                  '  d ... unsigned 32bit value (0...4294967295)';..
                  '  f ... single precision floating point';..
                  '  W ... like w, but with little/big endian transformation';..
                  '  D ... like d, but with little/big endian transformation';..
                  '  F ... like f, but with little/big endian transformation';..
                  '  _ ... space byte that is ignored'..
                  ],..
                 parametercaptions,..
                 list('str',1,'vec',1,'str',1,'str',1,'vec',1,'vec',1),..
                 exprs)
    parametercaptions=parametercaptionsorg
    if ~ok then break, end
    if ok then
      outputsignals=0;
      temp=ascii(outputformat)
      for i=1:max(size(temp))
        select temp(i)
          case ascii('b') then
            outputsignals=outputsignals+1;
          case ascii('w') then
            outputsignals=outputsignals+1;
          case ascii('d') then
            outputsignals=outputsignals+1;
          case ascii('f') then
            outputsignals=outputsignals+1;
          case ascii('W') then
            outputsignals=outputsignals+1;
          case ascii('D') then
            outputsignals=outputsignals+1;
          case ascii('F') then
            outputsignals=outputsignals+1;
          case ascii('_') then
          else
            parametercaptions(3)="*"+parametercaptions(3);
            ok=%f;
        end
      end
      inputsignals=0;
      temp=ascii(inputformat)
      for i=1:max(size(temp))
        select temp(i)
          case ascii('b') then
            inputsignals=inputsignals+1;
          case ascii('w') then
            inputsignals=inputsignals+1;
          case ascii('d') then
            inputsignals=inputsignals+1;
          case ascii('f') then
            inputsignals=inputsignals+1;
          case ascii('W') then
            inputsignals=inputsignals+1;
          case ascii('D') then
            inputsignals=inputsignals+1;
          case ascii('F') then
            inputsignals=inputsignals+1;
          case ascii('_') then
          else
            parametercaptions(4)="*"+parametercaptions(4);
            ok=%f;
        end
      end
    end
    if ok then
      in = ones(outputsignals,1)
      out = ones(inputsignals,1)
      [model, graphics, ok] = check_io(model, graphics, in, out, 1, [])
    end


    ipar=[baudrate,usethreading,threadingpriority,length(comport),ascii(comport),0,length(outputformat),ascii(outputformat),0,length(inputformat),ascii(inputformat),0];
    ipar=int(ipar(:));
    rpar=[];
    rpar=rpar(:);

    if ok then
      graphics.exprs = exprs;
      model.rpar = rpar;
      model.ipar = ipar;
      model.dstate = [1];
      x.graphics = graphics;
      x.model = model
      break
    end
    x_message(['Some parameters are not valid';'The parameters are marked with a *.']);
  end
case 'define' then			// initialize parameters
  comport = 'COM1';
  baudrate = 115200;
  outputformat = 'b';
  inputformat = 'bw';
  usethreading = 0;
  threadingpriority = 0;
  model = scicos_model()
  model.sim = list('wgserialxcosio_fnc', 4)

  model.in = ones(length(outputformat),1)
  model.out = ones(length(inputformat),1)

  ipar=[baudrate,usethreading,threadingpriority,length(comport),ascii(comport),0,length(outputformat),ascii(outputformat),0,length(inputformat),ascii(inputformat),0];
  ipar=int(ipar(:));
  rpar=[];
  rpar=rpar(:);

  model.evtin = 1
  model.rpar=rpar;
  model.ipar=ipar;
  model.dstate=[1];
  model.blocktype='d'

  model.dep_ut=[%f %f];
  exprs=[comport; sci2exp(baudrate); outputformat; inputformat; sci2exp(usethreading); sci2exp(threadingpriority)];
//  gr_i=['xstringb(orig(1),orig(2),[icon_line1; icon_line2],sz(1),sz(2),''fill'')']

  x = standard_define([5 3], model, [], []);
  x.graphics.in_implicit=[];
  x.graphics.style=[strcat(["blockWithLabel;verticalLabelPosition=bottom;verticalAlign=top;align=center;spacing=-3;displayedLabel=<b>Serial Xcos IO V";wgserialxcosio('getversion',0,0);"</b><small><br>Conn: %s:%s<BR>OUT(%s) IN(%s)<BR>Threading: %s (Prio %s)</small>"])];
  x.graphics.exprs=exprs;

case 'getversion' then
  x='16.04'

case 'getModulePath'  then
  etc_tlbx  = get_function_path("wgserialxcosio")
  etc_tlbx  = getshortpathname(etc_tlbx);
  root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\macros\wgserialxcosio.sci") );
  x = root_tlbx;
end

endfunction
