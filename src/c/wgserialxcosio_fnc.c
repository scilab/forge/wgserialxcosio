// *********************************************************************
// Serial Communication Interface Scicos-Block
// Copyright (C) 2016  Weichinger Klaus, snaky.1@gmx.at
// Project web-page: http://bioe.sourceforge.net
//
// License:  GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
// http://www.gnu.org/licenses/
// *********************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *********************************************************************
// Dedication
// **********
//
//  This small piece of code was developed before year
//  2012 and now i publish and dedicate it our beloved
//  son Gregor, who died suddenly 2015.
//
//     +-----+
//     |      \    #
//     |Gregor +---#--+
//    _|___ Weichinger \
//   /     \  2012-2015 |
//  |   O   |----/   \--+
//   \     /     \   /
//  ###############################


//#define USETHREADING
//#define MIRRORMODE // no serial used. Software bridging

#include <scicos_block4.h>
#include "scicos_malloc.h"
#include "scicos_free.h"
#include "scicosthreading.h"
#include "comport.h"
#include "binarysource.h"
#ifndef WIN32
  #include <unistd.h>
#endif

union DataTransformation
{
    struct
    {
        unsigned char b0;
        unsigned char b1;
        unsigned char b2;
        unsigned char b3;
    } buffer; // byte
    struct
    {
        unsigned char value;
        unsigned char dummy1;
        unsigned char dummy2;
        unsigned char dummy3;
    } b; // byte
    struct
    {
      unsigned short value;
      unsigned short dummy1;
    } w; // word
    struct
    {
      unsigned long value;
    } d; // double word
    struct
    {
      float value;
    } f;
};

struct SerialFIFO
{
  #define SerialFIFOSize 4096
  unsigned char buffer[SerialFIFOSize];
  int WriteIndex;
  int ReadIndex;
};

struct BlockData
{
  #define ConnectionNameSize 100
  #define OutputFormatSize 100
  #define InputFormatSize 100
  #define FrameMaxSize 100
  #define Base64FrameMaxSize 200
  int mirrormode;
  int connectionparam;
  int usethreading;
  int threadpriority;
  double debug;
  char connectionname[ConnectionNameSize];
  char numberOutputCharacters;
  char OutputFormat[OutputFormatSize];
  char numberInputCharacters;
  char InputFormat[InputFormatSize];
//  #ifndef MIRRORMODE
  struct ComPort comport;
//  #endif
//  #ifdef USETHREADING
  struct ScicosThread Thread;
//  #endif
  struct SerialFIFO WriteBuffer;
  struct SerialFIFO ReadBuffer;
  unsigned char RxFrame[FrameMaxSize];
  unsigned char TxFrame[FrameMaxSize];
  struct binarysource rx_source;
  struct binarysource tx_source;
  unsigned short OutputSize;
  unsigned short InputSize;
};

void SerialFIFOInit(struct SerialFIFO *fifo)
{
  fifo->WriteIndex=0;
  fifo->ReadIndex=0;
}

int SerialFIFOWrite(struct SerialFIFO *fifo, unsigned char *c)
{
  int NextIndex = ( fifo->WriteIndex + 1 ) % SerialFIFOSize;
  int res=-1;
  if ( NextIndex != fifo->ReadIndex )
  {
    res=0;
    fifo->buffer[fifo->WriteIndex] = *c;
    fifo->WriteIndex = NextIndex;
  }
  return res;
}

int SerialFIFORead(struct SerialFIFO *fifo, unsigned char *c)
{
  int NextIndex;
  int res=-1;
  if ( fifo->WriteIndex != fifo->ReadIndex )
  {
    res=0;
    NextIndex = ( fifo->ReadIndex + 1 ) % SerialFIFOSize;
    *c = fifo->buffer[fifo->ReadIndex];
    fifo->ReadIndex = NextIndex;
  }
  return res;
}

unsigned short base64comm_crc(unsigned short crc, unsigned char rx)
{
  const unsigned short base64comm_crc16_table[256] = {
  0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
  0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
  0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
  0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
  0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
  0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
  0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
  0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
  0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
  0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
  0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
  0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
  0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
  0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
  0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
  0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
  0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
  0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
  0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
  0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
  0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
  0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
  0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
  0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
  0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
  0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
  0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
  0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
  0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
  0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
  0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
  0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78};

  return ((crc >> 8) ^ base64comm_crc16_table[rx ^ (crc & 0xff)]);
}

int TaskStart( void *ctx )
{
  struct BlockData *blk = (struct BlockData*)ctx;
  if (blk->mirrormode==0)
  {
    ComPortOpen(&(blk->comport),blk->connectionname,blk->connectionparam);
  }
  return 0;
}

int TaskCyclicc( void *ctx )
{
  return 0;
}

int TaskCyclic( void *ctx )
{
  struct BlockData *blk = (struct BlockData*)ctx;
  unsigned char buffer[240];
  unsigned char tx;
  unsigned char rx;
  int length;
  int i;

  if (blk->mirrormode!=0)
  {
    while(SerialFIFORead(&(blk->WriteBuffer),&tx)==0)
    {
      SerialFIFOWrite(&(blk->ReadBuffer),&tx);
    }
  } else {
    length=0;
    while((SerialFIFORead(&(blk->WriteBuffer),&tx)==0)&&(length<200))
    {
      buffer[length]=tx;
      length++;
    }
    ComPortWrite(&(blk->comport),buffer,length);

    length=1;
    while(length>0)
    {
      length=ComPortRead(&(blk->comport),buffer,sizeof(buffer));
      if (length > 0 )
      {
        for(i=0;i<length;i++)
        {
          rx=buffer[i];
          SerialFIFOWrite(&(blk->ReadBuffer),&rx);
        }
      }
    }
  }
/*
  #ifdef unix
    usleep(1000);
  #endif
  #ifdef _WIN32
    Sleep(1);
  #endif
*/
  return 0;
}

int TaskEnd( void *ctx )
{
  struct BlockData *blk = (struct BlockData*)ctx;
  if (blk->mirrormode==0)
  {
    ComPortClose(&(blk->comport));
  }
  return 0;
}


static void init(scicos_block *block)
{
  struct BlockData *blk;
  int i;
  int size;
  int j;


  *block->work=scicos_malloc(sizeof(struct BlockData));

  blk=*block->work;
  blk->debug=0.2;
  blk->connectionparam = block->ipar[0];
  blk->usethreading = block->ipar[1];
  blk->threadpriority = block->ipar[2];
  i=3;

  // Copy the Com-Port Name
  j=0;
  size=block->ipar[i];
  i++;
  while( ( i<block->nipar ) && ( j<ConnectionNameSize-1) && ( block->ipar[i]!='\0' ) )
  {
    blk->connectionname[j]=block->ipar[i];
    j++;
    i++;
  }
  blk->connectionname[j]='\0';
  i++;

  // Copy Output-Format
  j=0;
  blk->OutputSize=0;
  size=block->ipar[i];
  i++;
  while( ( i<block->nipar ) && ( j<OutputFormatSize-1) && ( block->ipar[i]!='\0' ) )
  {
    blk->OutputFormat[j]=block->ipar[i];
    switch(blk->OutputFormat[j])
    {
      case 'b':
        blk->OutputSize+=1;
        break;
      case 'w':
        blk->OutputSize+=2;
         break;
      case 'd':
        blk->OutputSize+=4;
        break;
      case 'f':
        blk->OutputSize+=4;
        break;
      case 'W':
        blk->OutputSize+=2;
         break;
      case 'D':
        blk->OutputSize+=4;
        break;
      case 'F':
        blk->OutputSize+=4;
        break;
      case '_':
        blk->OutputSize+=1;
        break;
    }
    j++;
    i++;
  }
  blk->OutputFormat[j]='\0';
  blk->numberOutputCharacters=j;
  i++;

  // Copy Input-Format
  j=0;
  blk->InputSize=0;
  size=block->ipar[i];
  i++;
  while( ( i<block->nipar ) && ( j<InputFormatSize-1) && ( block->ipar[i]!='\0' ) )
  {
    blk->InputFormat[j]=block->ipar[i];
    switch(blk->InputFormat[j])
    {
      case 'b':
        blk->InputSize+=1;
        break;
      case 'w':
        blk->InputSize+=2;
         break;
      case 'd':
        blk->InputSize+=4;
        break;
      case 'f':
        blk->InputSize+=4;
        break;
      case 'W':
        blk->InputSize+=2;
         break;
      case 'D':
        blk->InputSize+=4;
        break;
      case 'F':
        blk->InputSize+=4;
        break;
      case '_':
        blk->InputSize+=1;
        break;
    }
    j++;
    i++;
  }
  blk->InputFormat[j]='\0';
  blk->numberInputCharacters=j;
  i++;

  if (strncmp(blk->connectionname,"mirror",6)==0)
  {
    blk->mirrormode=1;
  }
  else
  {
    blk->mirrormode=0;
  }
  SerialFIFOInit(&(blk->WriteBuffer));
  SerialFIFOInit(&(blk->ReadBuffer));

  if (blk->usethreading!=0)
  {
    CreateScicosThread(&(blk->Thread),TaskStart,TaskCyclic,TaskEnd,blk);
  }
  else
  {
    if (blk->mirrormode==0)
    {
      ComPortOpen(&(blk->comport),blk->connectionname,blk->connectionparam);
    }
  }
}


static void in(scicos_block *block)
{
  struct BlockData *blk;
  double *u;
  double signal;
  unsigned char tx;
  int j;
  int i;
  int signalindex;
  int len;
  int out;
  unsigned char res;
  unsigned short crc;
  unsigned char txbuffer[255];
  int txlength;
  int length;
  unsigned char buffer[200];

  union DataTransformation data;

  blk=*block->work;

  j=0;
  signalindex=0;
  for(i=0;i<blk->numberOutputCharacters;i++)
  {
    u=block->inptr[signalindex];
    switch(blk->OutputFormat[i])
    {
      case 'b':
        data.b.value=(unsigned char) (u[0]+0.5);
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        signalindex++;
        break;

      case 'w':
        data.w.value=(unsigned short) (u[0]+0.5);
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        blk->TxFrame[j]=data.buffer.b1;
        j++;
        signalindex++;
        break;

      case 'd':
        data.d.value=(unsigned long) (u[0]+0.5);
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        blk->TxFrame[j]=data.buffer.b1;
        j++;
        blk->TxFrame[j]=data.buffer.b2;
        j++;
        blk->TxFrame[j]=data.buffer.b3;
        j++;
        signalindex++;
        break;

      case 'f':
        data.f.value=(float) (u[0]);
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        blk->TxFrame[j]=data.buffer.b1;
        j++;
        blk->TxFrame[j]=data.buffer.b2;
        j++;
        blk->TxFrame[j]=data.buffer.b3;
        j++;
        signalindex++;
        break;

      case 'W':
        data.w.value=(unsigned short) (u[0]+0.5);
        blk->TxFrame[j]=data.buffer.b1;
        j++;
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        signalindex++;
        break;

      case 'D':
        data.d.value=(unsigned long) (u[0]+0.5);
        blk->TxFrame[j]=data.buffer.b3;
        j++;
        blk->TxFrame[j]=data.buffer.b2;
        j++;
        blk->TxFrame[j]=data.buffer.b1;
        j++;
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        signalindex++;
        break;

      case 'F':
        data.f.value=(float) (u[0]);
        blk->TxFrame[j]=data.buffer.b3;
        j++;
        blk->TxFrame[j]=data.buffer.b2;
        j++;
        blk->TxFrame[j]=data.buffer.b1;
        j++;
        blk->TxFrame[j]=data.buffer.b0;
        j++;
        signalindex++;
        break;

      case '_':
        blk->TxFrame[j]=0;
        j++;
        break;
    }
  }
  crc=0x43A6;
  for(i=0;i<j;i++)
  {
    crc=base64comm_crc(crc,blk->TxFrame[i]);
  }
  data.w.value=crc;
  blk->TxFrame[j]=data.buffer.b0;
  j++;
  blk->TxFrame[j]=data.buffer.b1;
  j++;
  if (blk->OutputSize+2 == j)
  {
    txlength=0;
    txbuffer[txlength]='<';
    txlength++;
    binarysource_init(&(blk->tx_source),blk->TxFrame,j);
    while((res=binarysource_getnextbase64(&(blk->tx_source),&tx))==BINARYSOURCE_OK)
    {
      txbuffer[txlength]=tx;
      txlength++;
    }
    txbuffer[txlength]='>';
    txlength++;
    for(i=0;i<txlength;i++)
    {
      SerialFIFOWrite(&(blk->WriteBuffer),&(txbuffer[i]));
    }
  }
  if (blk->usethreading==0)
  {
    if (blk->mirrormode!=0)
    {
      while((SerialFIFORead(&(blk->WriteBuffer),&tx)==0))
      {
        SerialFIFOWrite(&(blk->ReadBuffer),&tx);
      }
    } else {
      length=0;
      while((SerialFIFORead(&(blk->WriteBuffer),&tx)==0))
      {
        buffer[length]=tx;
        length++;
        if (length==sizeof(buffer))
        {
          ComPortWrite(&(blk->comport),buffer,length);
          length=0;
        }
      }
      if (length>0)
      {
        ComPortWrite(&(blk->comport),buffer,length);
      }
    }
  }
}

static void out(scicos_block *block)
{
  struct BlockData *blk;
  double *y;
  unsigned char rx;
  int j,i,signalindex;
  union DataTransformation data;
  unsigned short crc;
  int length;
  unsigned char buffer[40];
  blk=*block->work;
  if (blk->usethreading==0)
  {
    if (blk->mirrormode==0)
    {
      length=ComPortRead(&(blk->comport),buffer,sizeof(buffer));
      while (length > 0 )
      {
        for(i=0;i<length;i++)
        {
          rx=buffer[i];
          SerialFIFOWrite(&(blk->ReadBuffer),&rx);
        }
        length=ComPortRead(&(blk->comport),buffer,sizeof(buffer));
      }
    }
  }

  while ( SerialFIFORead(&(blk->ReadBuffer),&rx) == 0 )
  {
    if (rx=='<')
    {
      binarysource_init(&(blk->rx_source),blk->RxFrame,FrameMaxSize);
    }
    else if (rx=='>' && (blk->InputSize+2) == binarysource_getlength(&(blk->rx_source)))
    {
      crc=0x43A6;
      j=blk->InputSize;
      for(i=0;i<j;i++)
      {
        crc=base64comm_crc(crc,blk->RxFrame[i]);
      }
      data.buffer.b0=blk->RxFrame[j];
      data.buffer.b1=blk->RxFrame[j+1];
      if (data.w.value == crc)
      {
        // analyse
        j=0;
        signalindex=0;
        for(i=0;i<blk->numberInputCharacters;i++)
        {
          y=block->outptr[signalindex];
          switch(blk->InputFormat[i])
          {
            case 'b':
              data.buffer.b0=blk->RxFrame[j];
              j++;
              y[0]=data.b.value;
              signalindex++;
              break;

            case 'w':
              data.buffer.b0=blk->RxFrame[j];
              j++;
              data.buffer.b1=blk->RxFrame[j];
              j++;
              y[0]=data.w.value;
              signalindex++;
              break;

            case 'd':
              data.buffer.b0=blk->RxFrame[j];
              j++;
              data.buffer.b1=blk->RxFrame[j];
              j++;
              data.buffer.b2=blk->RxFrame[j];
              j++;
              data.buffer.b3=blk->RxFrame[j];
              j++;
              y[0]=data.d.value;
              signalindex++;
              break;

            case 'f':
              data.buffer.b0=blk->RxFrame[j];
              j++;
              data.buffer.b1=blk->RxFrame[j];
              j++;
              data.buffer.b2=blk->RxFrame[j];
              j++;
              data.buffer.b3=blk->RxFrame[j];
              j++;
              signalindex++;
              y[0]=data.f.value;

              break;

            case 'W':
              data.buffer.b1=blk->RxFrame[j];
              j++;
              data.buffer.b0=blk->RxFrame[j];
              j++;
              y[0]=data.w.value;
              signalindex++;
              break;

            case 'D':
              data.buffer.b3=blk->RxFrame[j];
              j++;
              data.buffer.b2=blk->RxFrame[j];
              j++;
              data.buffer.b1=blk->RxFrame[j];
              j++;
              data.buffer.b0=blk->RxFrame[j];
              j++;
              y[0]=data.d.value;
              signalindex++;
              break;

            case 'F':
              data.buffer.b3=blk->RxFrame[j];
              j++;
              data.buffer.b2=blk->RxFrame[j];
              j++;
              data.buffer.b1=blk->RxFrame[j];
              j++;
              data.buffer.b0=blk->RxFrame[j];
              j++;
              y[0]=data.f.value;
              signalindex++;
              break;
            case '_':
              j++;
              break;
          }
        }
      }
    }
    else
    {
      binarysource_putnextbase64(&(blk->rx_source),rx);
    }
  }
//  y=block->outptr[0];
//  y[0]=blk->debug;
}

static void end(scicos_block *block)
{
  struct BlockData *blk;
  blk=*block->work;
  if (blk->usethreading!=0)
  {
    StopScicosThread(&(blk->Thread));
    CloseScicosThread(&(blk->Thread));
    #ifdef _WIN32
      Sleep(300);
    #else
      usleep(300000u);
    #endif
  }
  else
  {
    if (blk->mirrormode==0) ComPortClose(&(blk->comport));
  }
  scicos_free(*block->work);
}

void wgserialxcosio_fnc(scicos_block *block,int flag)
{
  if (flag==1)
  {          /* set output */
//    printf("a");
    out(block);
  }
  if (flag==2)
  {          /* get input */
//    printf("b");
    in(block);
  }
  else if (flag==5)
  {     /* termination */
//    printf("c");
    end(block);
  }
  else if (flag ==4)
  {    /* initialisation */
//    printf("d");
    init(block);
  }
}
