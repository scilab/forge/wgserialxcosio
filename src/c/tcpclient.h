// *********************************************************************
// Threading Library
// Copyright (C) 2016  Weichinger Klaus, snaky.1@gmx.at
// Project web-page: http://bioe.sourceforge.net
//
// License:  GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
// http://www.gnu.org/licenses/
// *********************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *********************************************************************
// Dedication
// **********
//
//  This small piece of code was developed before year
//  2012 and now i publish and dedicate it our beloved
//  son Gregor, who died suddenly 2015.
//
//     +-----+
//     |      \    #
//     |Gregor +---#--+
//    _|___ Weichinger \
//   /     \  2012-2015 |
//  |   O   |----/   \--+
//   \     /     \   /
//  ###############################

#ifndef _tcpclient_h_
#define _tcpclient_h_

#ifndef _WIN32
  #include <fcntl.h>
  #include <unistd.h>
  #include <termios.h>
  struct baud_rates {
    int rate;
    unsigned int cflag;
  };
  static const struct baud_rates baud_rates[] = {
           {1152000, B1152000 },
           {1000000, B1000000 },
           { 921600, B921600 },
           { 576000, B576000 },
           { 500000, B500000 },
           { 460800, B460800 },
           { 230400, B230400 },
           { 115200, B115200 },
           {  57600, B57600  },
           {  38400, B38400  },
           {  19200, B19200  },
           {   9600, B9600   },
           {   4800, B4800   },
           {   2400, B2400   },
           {   1200, B1200   },
           {    600, B600   },
           {    300, B300   },
           {    200, B200   },
           {    150, B150   },
           {    134, B134   },
           {    110, B110   },
           {     75, B75   },
           {     50, B50   },
           {      0, B38400  }
 };

  struct ComPort
  {
    int Handle;
  };

  static int ComPortOpen(struct ComPort *comport, const char *devicename, int baudrate)
  {
    struct termios options;
    int i;
    memset(&options,0,sizeof(options));
    comport->Handle = open(devicename, O_RDWR | O_NOCTTY | O_NDELAY);
    if (comport->Handle == -1)
    {
      // error opening com-port
      return -1;
    }
    fcntl(comport->Handle, F_SETFL, FNDELAY);
    tcgetattr(comport->Handle, &options);
    for (i = 0; baud_rates[i].rate>0; i++)
    {
      if (baud_rates[i].rate <= baudrate)
      {
        cfsetispeed(&options, baud_rates[i].cflag);
        cfsetospeed(&options, baud_rates[i].cflag);
        break;
      }
    }
    options.c_cflag = (options.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    options.c_iflag &= ~IGNBRK;         // disable break processing
    options.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    options.c_oflag = 0;                // no remapping, no delays
    options.c_cc[VMIN]  = 0;            // read doesn't block
    options.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    options.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    options.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    options.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    options.c_cflag |= 0; // parity
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CRTSCTS;
    tcsetattr(comport->Handle, TCSANOW, &options);
  }

  static int ComPortWrite(struct ComPort *comport, const unsigned char *buf, int len)
  {
    unsigned long retlen;
    retlen = write(comport->Handle,buf,len);
    if (retlen<0)
    {
      return -1;
    }
    return (int) retlen;
  }

  static int ComPortRead(struct ComPort *comport, unsigned char *buf, int len)
  {
    unsigned long retlen;
    retlen = read(comport->Handle,buf,len);
    if (retlen > 0)
    {
      return (int) retlen;
    }
    return -1;
  }

  static int ComPortClose(struct ComPort *comport)
  {
    close(comport->Handle);
    return 0;
  }
#endif

#ifdef _WIN32
  #include <windows.h>
  #include <stdio.h>

  struct ComPort
  {
    HANDLE Handle;
  };

  static int ComPortOpen(struct ComPort *comport, const char *devicename, int baudrate)
  {
    char devname[100];
    COMMCONFIG cc;
    COMMTIMEOUTS ctimeout;
    DWORD ccsize;
    int ret;
    int timeout = 1;

    if (strlen(devicename) > 80) return -1;
    sprintf(devname,"\\\\.\\%s",devicename);
    comport->Handle = CreateFile(  devname,
                                GENERIC_READ | GENERIC_WRITE,
                                0,
                                0,
                                OPEN_EXISTING,
                                0,
                                0
                                );
    if (comport->Handle == INVALID_HANDLE_VALUE)
    {
      return -1;
    }
    SetupComm(comport->Handle,2048,2048);
    GetCommConfig(comport->Handle,&cc,&ccsize);
    cc.dcb.DCBlength           = sizeof(DCB);
    cc.dcb.BaudRate            = baudrate;
    cc.dcb.fBinary             = 1;
    cc.dcb.fParity             = 1;
    cc.dcb.fOutxCtsFlow        = 0;
    cc.dcb.fOutxDsrFlow        = 0;
    cc.dcb.fDtrControl         = DTR_CONTROL_DISABLE;
    cc.dcb.fTXContinueOnXoff   = 1;
    cc.dcb.fNull               = 0;
    cc.dcb.fRtsControl         = RTS_CONTROL_DISABLE;
    cc.dcb.fAbortOnError       = 0;
    cc.dcb.ByteSize            = 8;
    cc.dcb.Parity              = 0;
    cc.dcb.StopBits            = ONESTOPBIT;
    cc.dwProviderSubType       = PST_RS232;

    ret = SetCommConfig(comport->Handle,&cc,sizeof(cc));
    if (ret == 0) return -1;

    ctimeout.ReadIntervalTimeout           = MAXDWORD;
    ctimeout.ReadTotalTimeoutMultiplier    = 0;
    ctimeout.ReadTotalTimeoutConstant      = 0;
//    ctimeout.WriteTotalTimeoutMultiplier   = 12000L / baudrate;
    ctimeout.WriteTotalTimeoutMultiplier   = 0;
    ctimeout.WriteTotalTimeoutConstant     = 0;
    SetCommTimeouts(comport->Handle,&ctimeout);
    return 0;
  }

  static int ComPortWrite(struct ComPort *comport, const unsigned char *buf, int len)
  {
    BOOL ret;
    unsigned long retlen;
    ret = WriteFile(  comport->Handle,
                      buf,
                      len,
                      &retlen,
                      NULL
                      );
    if (ret) {
      FlushFileBuffers(comport->Handle);
      return (int) retlen;
    }
    return -1;
  }

  static int ComPortRead(struct ComPort *comport, unsigned char *buf, int len)
  {
    BOOL ret;
    unsigned long retlen;
    ret = ReadFile( comport->Handle,
                    buf,
                    len,
                    &retlen,
                    NULL
                    );
    if (retlen > 0)
    {
      return (int) retlen;
    }
    return -1;
  }

  static int ComPortClose(struct ComPort *comport)
  {
    CloseHandle(comport->Handle);
    return 0;
  }
#endif

#endif
