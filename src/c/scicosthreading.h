/*********************************************
 * Threading library that also can be used
 * within a SCICOS block, WIN32/UNIX
 * Copyright (C) 2012  Weichinger Klaus, snaky.1@gmx.at
 * Project web-page: bioe.sourceforge.net
 *
 * License: CC-GNU GPL
 * http://creativecommons.org/licenses/GPL/2.0/
 *********************************************
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *********************************************/

#ifndef _scicosthreading_h_
#define _scicosthreading_h_

#ifdef _WIN32
  #include <windows.h>
#else
  #include <pthread.h>
#endif

typedef int (*ThreadCallBackFunction) (void*);

struct ScicosThread
{
  int TerminateFlag;
#ifdef _WIN32
  HANDLE Handle;
#else
  pthread_t thrd;
#endif
  ThreadCallBackFunction StartFunctionPointer;
  ThreadCallBackFunction CyclicFunctionPointer;
  ThreadCallBackFunction EndFunctionPointer;
  void * DataPointer;
};

#ifdef _WIN32
  static DWORD WINAPI SciosThreadingMainFunction( LPVOID lpParam )
  {
    int res=0;
    struct ScicosThread *thread = (struct ScicosThread*) lpParam;
    SetThreadPriority(GetCurrentThread(),31);
#else
  static void *SciosThreadingMainFunction(void *arg)
  {
    int res=0;
    struct ScicosThread *thread = (struct ScicosThread*) arg;
    struct sched_param param;
//    param.sched_priority = 10;
//    if(res==0 && sched_setscheduler(0, SCHED_FIFO, &param)==-1) res=-1;
#endif
    if (res==0 && thread->StartFunctionPointer!=NULL)
    {
      if (thread->StartFunctionPointer(thread->DataPointer)!=0) res=-2;
    }
    if (res==0 && thread->CyclicFunctionPointer!=NULL)
    {
      while((thread->TerminateFlag==0) && (res==0))
      {
        if (thread->CyclicFunctionPointer(thread->DataPointer)!=0) res=-3;
      }
    }
    if (res==0 && thread->EndFunctionPointer!=NULL)
    {
      if (thread->EndFunctionPointer(thread->DataPointer)!=0) res=-4;
    }
#ifdef _WIN32
    return 0;
#else
    return NULL;
#endif
  }

  static int CreateScicosThread( struct ScicosThread *thread, ThreadCallBackFunction StartFunction, ThreadCallBackFunction CyclicFunction, ThreadCallBackFunction EndFunction, void *DataPointer)
  {
    thread->TerminateFlag=0;
    thread->StartFunctionPointer=StartFunction;
    thread->CyclicFunctionPointer=CyclicFunction;
    thread->EndFunctionPointer=EndFunction;
    thread->DataPointer=DataPointer;
#ifdef _WIN32
      thread->Handle = CreateThread( NULL, 0, SciosThreadingMainFunction, thread, CREATE_SUSPENDED,NULL);
      if ( thread->Handle == NULL)
      {
        ExitProcess(thread->Handle);
        return -1;
      }
      else
      {
        if (ResumeThread(thread->Handle)!=0)
        {
          return -1;
        }
        else
        {
          return 0;
        }
      }
#else
      if (pthread_create(&(thread->thrd), NULL, SciosThreadingMainFunction, thread)!=0)
      {
        return -1;
      }
      else
      {
        return 0;
      }
#endif
  }

  static void WaitForScicosThread( struct ScicosThread *thread )
  {
#ifdef _WIN32
      HANDLE Array_Of_Thread_Handles[1];
      Array_Of_Thread_Handles[0]=thread->Handle;
      WaitForMultipleObjects(1,Array_Of_Thread_Handles,TRUE,INFINITE);
#else
      pthread_join(thread->thrd,NULL);
#endif
  }

  static int StopScicosThread( struct ScicosThread *thread )
  {
    thread->TerminateFlag=1;
    WaitForScicosThread(thread);
    return 0;
  }

  static void CloseScicosThread( struct ScicosThread *thread )
  {
#ifdef _WIN32
      CloseHandle(thread->Handle);
#endif
  }

#endif
